INSERT INTO banks(name, address, commission_other_banks, minimum_daily_limit, maximum_daily_limit, created_at) VALUES 
	('Banext','Madrid', 2, 500, 6000, NOW()),
	('Banluinel','Madrid', 3, 400, 3000, NOW());

INSERT INTO branches(name, address, bank_id, created_at) VALUES 
	('Banext Acacias','Av. Acacias', 1, NOW()),
	('Banluinel Retiro','Retiro', 2, NOW()),
	('Banext Vallecas','Vallecas', 1, NOW());

INSERT INTO atms(serial_number, branch_id, created_at) VALUES 
	('B1001',1, NOW()),
	('B1002',1, NOW()),
	('B2001',2, NOW()),
	('B3001',3, NOW());

INSERT INTO accounts(iban, type, balance, branch_id, enabled, created_at) VALUES 
	('ES9121000418450200051332', 'Checking', 180, 1, 1, NOW()),
	('ES9121000418450200051310', 'Checking', 0, 1, 1, NOW()),
	('ES9121000418450200052311', 'Checking', 0, 1, 1, NOW()),
	('ES9121000418450200051345', 'Checking', 0, 2, 1, NOW());

INSERT INTO customers(name, lastname, document, enabled, created_at) VALUES
	('Luinel', 'Andrade', 'Y7506663V', 1, NOW()),
	('Juan', 'Perez', 'P123456789', 1, NOW()),
	('Luis', 'Sanchez', 'P145672345', 1, NOW());

INSERT INTO accounts_customers(account_id, customer_id) VALUES
	(1,1),
	(2,1),
	(3,2),
	(4,3);

INSERT INTO transaction_types(description, created_at) VALUES
	('Cash Deposit', NOW()),
	('Cash Withdrawal', NOW()),
	('Transfer Sent', NOW()),
	('Transfer Received', NOW()),
	('ATM fee', NOW());

INSERT INTO transactions( amount, account_id, transaction_type_id, created_at) VALUES
	(100, 1, 1, '2020-10-01 07:00:00'),
	(-20, 1, 3, '2020-11-05 09:00:00'),
	(-10, 1, 2, '2020-12-20 10:00:00'),
	(90,1,1, '2021-01-10 11:00:00'),
	(20,1,1, NOW());

INSERT INTO cards_types(description, created_at) VALUES
	('Debit', NOW()),
	('Credit', NOW());

INSERT INTO cards( number_card, expiration_date, cvc, enabled, pin, initial_activation, daily_limit, card_type_id, account_id, customer_id, created_at ) VALUES
	( '2000349010901234', '12/24', '160', 1, '$2a$10$9zKZRPNcZrVI73eUR9RCPuWqRUijvZryfzgoohSuLOfGYcCBRqS.O', 1, 600, 1, 1, 1, NOW() ),
	( '2100349010101201', '07/22', '100', 1, '$2a$10$9zKZRPNcZrVI73eUR9RCPuWqRUijvZryfzgoohSuLOfGYcCBRqS.O', 1, 800, 1, 3, 2, NOW() );


