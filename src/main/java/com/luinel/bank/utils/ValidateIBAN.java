package com.luinel.bank.utils;

import org.springframework.stereotype.Component;

@Component
public class ValidateIBAN {
	
	/**
	 * Function to simulate IBAN validation
	 */
	public Boolean validIban( String iban ) {
		
		int IBAN_MIN_SIZE = 15;
	    int IBAN_MAX_SIZE = 30;

	    String trimmed = iban.trim();

	    if (trimmed.length() < IBAN_MIN_SIZE || trimmed.length() > IBAN_MAX_SIZE) {
	        return false;
	    }

	    return true;
	}
}
