package com.luinel.bank.utils;

public class TransactionTypeConstants {

	// descriptions types of transactions

	public static final String CASH_DEPOSIT = "Cash Deposit";
	public static final String CASH_WITHDRAWAL = "Cash Withdrawal";
	public static final String TRANSFER_SENT = "Transfer Sent";
	public static final String TRANSFER_RECEIVED = "Transfer Received";
	public static final String ATM_FEE = "ATM fee";
}
