package com.luinel.bank.utils;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class Dates {
	
	public Date getCurrentDateMinimumTime() {
		Calendar current = Calendar.getInstance();
		current.set(current.get(Calendar.YEAR),current.get(Calendar.MONTH),current.get(Calendar.DATE),0,0,0);
		return current.getTime();
	}
	
	public Date getCurrentDateMaximumTime() {
		Calendar current = Calendar.getInstance();
		current.set(current.get(Calendar.YEAR),current.get(Calendar.MONTH),current.get(Calendar.DATE),23,59,59);
		return current.getTime();
	}
}
