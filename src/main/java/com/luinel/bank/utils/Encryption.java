package com.luinel.bank.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class Encryption {
	
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public Encryption() {
		this.bCryptPasswordEncoder = new BCryptPasswordEncoder();
	}

	public String encrypt(String string) {
		return bCryptPasswordEncoder.encode( string );
	}
	
	public Boolean matching(String pin, String pinEncoded) {
		return bCryptPasswordEncoder.matches(pin, pinEncoded);
	}
}
