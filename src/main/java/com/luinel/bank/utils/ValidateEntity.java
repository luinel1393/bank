package com.luinel.bank.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

@Component
public class ValidateEntity {
	public Map<String, Object> validation( BindingResult result){
		Map<String, Object> errors = new HashMap<>();
		result.getFieldErrors().forEach( err -> {
			errors.put(err.getField(),err.getDefaultMessage());
		});
		return errors;
	}
}
