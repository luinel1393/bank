package com.luinel.bank.responses;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

public class MessageResponse<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String message;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private T data;
	
	public MessageResponse(String message) {
		this.message = message;
	}

	public MessageResponse(String message, T data) {
		this.message = message;
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
