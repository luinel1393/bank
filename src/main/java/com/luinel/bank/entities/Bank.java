package com.luinel.bank.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "banks")
public class Bank {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	@Size(max = 100)
	private String name;
	
	@NotEmpty
	@Size(max = 100)
	private String address;
	
	@NotEmpty
	private int commissionOtherBanks;
	
	@NotEmpty
	private int minimumDailyLimit;
	
	@NotEmpty
	private int maximumDailyLimit;
	
	@OneToMany(mappedBy = "bank", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Branch> branches;
	
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date createdAt;

	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date updatedAt;
	
	@PrePersist
	public void prePersist() {
		this.createdAt = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getCommissionOtherBanks() {
		return commissionOtherBanks;
	}

	public void setCommissionOtherBanks(int commissionOtherBanks) {
		this.commissionOtherBanks = commissionOtherBanks;
	}

	public int getMinimumDailyLimit() {
		return minimumDailyLimit;
	}

	public void setMinimumDailyLimit(int minimumDailyLimit) {
		this.minimumDailyLimit = minimumDailyLimit;
	}

	public int getMaximumDailyLimit() {
		return maximumDailyLimit;
	}

	public void setMaximumDailyLimit(int maximumDailyLimit) {
		this.maximumDailyLimit = maximumDailyLimit;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<Branch> getBranches() {
		return branches;
	}

	public void setBranches(List<Branch> branches) {
		this.branches = branches;
	}
	
	
}
