package com.luinel.bank.security;

import static com.luinel.bank.security.Constants.LOGIN_URL;
import static com.luinel.bank.security.Constants.ACTIVATE_CARD_URL;
import static com.luinel.bank.security.Constants.ATM_OPERATIONS_URL;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
			.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
			.authorizeRequests()
			.antMatchers(HttpMethod.POST, LOGIN_URL).permitAll()
			.antMatchers(HttpMethod.POST, ACTIVATE_CARD_URL).permitAll()
			.antMatchers( ATM_OPERATIONS_URL ).authenticated();
	}
}