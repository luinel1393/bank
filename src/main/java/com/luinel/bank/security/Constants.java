package com.luinel.bank.security;

public class Constants {
	
		// Spring Security and JWT

		public static final String LOGIN_URL = "/api/v1/atms/operations/login";
		public static final String ACTIVATE_CARD_URL = "/api/v1/atms/operations/card/activate";
		public static final String ATM_OPERATIONS_URL = "/api/v1/atms/operations/**";
		public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
		public static final String TOKEN_BEARER_PREFIX = "Bearer ";
		public static final String SUPER_SECRET_KEY = "1234";
		public static final long TOKEN_EXPIRATION_TIME = 300000; // 5 minutes
}
