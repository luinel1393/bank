package com.luinel.bank.security;

import static com.luinel.bank.security.Constants.HEADER_AUTHORIZACION_KEY;
import static com.luinel.bank.security.Constants.SUPER_SECRET_KEY;
import static com.luinel.bank.security.Constants.TOKEN_BEARER_PREFIX;
import static com.luinel.bank.security.Constants.TOKEN_EXPIRATION_TIME;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JWTHelper {

	/**
	 * Generate token
	 */
	public String generateJWTToken(String numberCard, Map<String, Object> claims) {

		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		String token = Jwts.builder()
				.setId(UUID.randomUUID().toString())
				.setSubject(numberCard)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.addClaims(claims)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + TOKEN_EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SUPER_SECRET_KEY.getBytes()).compact();

		return TOKEN_BEARER_PREFIX + token;
	}

	/**
	 * Validate the token
	 */
	public Claims validateToken(HttpServletRequest request) {
		String jwtToken = request.getHeader(HEADER_AUTHORIZACION_KEY).replace(TOKEN_BEARER_PREFIX, "");
		return Jwts.parser().setSigningKey(SUPER_SECRET_KEY.getBytes()).parseClaimsJws(jwtToken).getBody();
	}

	/**
	 * Method to add the necessary configuration to the Spring context to authorize
	 * the request
	 */
	public void setUpSpringAuthentication(Claims claims) {
		@SuppressWarnings("unchecked")
		List<String> authorities = (List<String>) claims.get("authorities");

		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null,
				authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
		SecurityContextHolder.getContext().setAuthentication(auth);

	}

	/**
	 * Check the existence of token
	 */
	public boolean existsJWTToken(HttpServletRequest request, HttpServletResponse res) {
		String authenticationHeader = request.getHeader(HEADER_AUTHORIZACION_KEY);
		if (authenticationHeader == null || !authenticationHeader.startsWith(TOKEN_BEARER_PREFIX))
			return false;
		return true;
	}

	/**
	 *  Get the value of a token key
	 */
	public String getClaimValueFromToken(HttpServletRequest request, String key) {
		String value;
		try {
			Claims claims = validateToken(request);
			value = claims.get(key).toString();	
		} catch (Exception e) {
			value = null;
		}
		return value;
	}
}
