package com.luinel.bank.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luinel.bank.entities.Branch;
import com.luinel.bank.responses.MessageResponse;
import com.luinel.bank.services.ATMService;
import com.luinel.bank.services.AccountService;
import com.luinel.bank.services.BranchService;

@RestController
@RequestMapping(path = "/api/v1")
public class BranchController {
	@Autowired
	private BranchService branchService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private ATMService atmService;
	
	@GetMapping("/branches/{branchId}/accounts")
	public ResponseEntity<?> getAccountsByBranchdIdPages( @PathVariable Long branchId, Pageable pageable ) {
		Optional<Branch> branch = branchService.findById(branchId);
		if( branch.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Branch not found"), HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(accountService.findByBranchIdAndEnabledTrue(branchId,pageable), HttpStatus.OK);
	}
	
	@GetMapping("/branches/{branchId}/atms")
	public ResponseEntity<?> getATMsByBranchdIdPages( @PathVariable Long branchId, Pageable pageable ) {
		Optional<Branch> branch = branchService.findById(branchId);
		if( branch.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Branch not found"), HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(atmService.findByBranchId(branchId,pageable), HttpStatus.OK);
	}
}
