package com.luinel.bank.controllers;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luinel.bank.entities.Account;
import com.luinel.bank.entities.Customer;
import com.luinel.bank.responses.MessageResponse;
import com.luinel.bank.services.AccountService;
import com.luinel.bank.services.CustomerService;
import com.luinel.bank.utils.ValidateEntity;

@RestController
@RequestMapping(path = "/api/v1")
public class AccountController {
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private ValidateEntity validateEntity;
	
	@GetMapping("/accounts/all")
	public ResponseEntity<?> getAllAccounts() {
		return new ResponseEntity<>(accountService.findByEnabledTrue(), HttpStatus.OK);
	}
	
	@GetMapping("/accounts")
	public ResponseEntity<?> getAccountsPages( Pageable pageable ) {
		return new ResponseEntity<>(accountService.findByEnabledTrueByPages(pageable), HttpStatus.OK);
	}

	@GetMapping("/accounts/{id}")
	public ResponseEntity<?> getAccount( @PathVariable Long id ) {
		Optional<Account> account = accountService.findById(id);
		if( account.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Account not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(account.get(), HttpStatus.OK);
	}
	
	@PostMapping("/accounts")
	public ResponseEntity<?> createAccount(@Valid @RequestBody Account account, BindingResult result ){
		
		Boolean existsAccount = accountService.existsByIban( account.getIban() );
		if (result.hasErrors() || existsAccount ) {
			
			Map<String, Object> errores = validateEntity.validation(result);
			if(existsAccount) errores.put("iban", "IBAN already exists");
			
			return new ResponseEntity<>( new MessageResponse<>("Error creating account.", errores), HttpStatus.BAD_REQUEST);
		}

		try {
			account.setEnabled(true);
			return new ResponseEntity<>( accountService.save(account), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/accounts/{id}")
	public ResponseEntity<?> updateAccount( @Valid @RequestBody Account account, BindingResult result, @PathVariable Long id ){
		if (result.hasErrors() ) {
			return new ResponseEntity<>( new MessageResponse<>("Error updating account.", validateEntity.validation(result) ), HttpStatus.BAD_REQUEST);
		}

		Optional<Account> accountOptional = accountService.findById(id);
		if (accountOptional.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("Account not found"), HttpStatus.NOT_FOUND);
		}else {
			Optional<Account> otherAccount = accountService.findByIban(account.getIban());
			if( otherAccount.isPresent() && !otherAccount.get().getId().equals( id ) ) {
				return new ResponseEntity<>(new MessageResponse<>("IBAN already exists in another account."), HttpStatus.BAD_REQUEST);
			}
		}
		
		Account accountDB = accountOptional.get();
		accountDB.setIban(account.getIban());
		accountDB.setType( account.getType() );
		accountDB.setBalance( account.getBalance() );
		accountDB.setUpdatedAt(new Date());
		
		try {
			return new ResponseEntity<>( accountService.save(accountDB) , HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/accounts/{id}")
	public ResponseEntity<?> deleteAccount( @PathVariable Long id ) {
		Optional<Account> accountOptional = accountService.findById(id);
		if (accountOptional.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("Account not found"), HttpStatus.NOT_FOUND);
		}
		
		Account accountDB = accountOptional.get();
		accountDB.setEnabled(false);
		accountDB.setUpdatedAt(new Date());
		
		try {
			accountService.save(accountDB);
			return new ResponseEntity<>( new MessageResponse<>("Successfully deleted account."), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/accounts/{accountId}/customers")
	public ResponseEntity<?> getCustomersByAccountIdPages( @PathVariable Long accountId, Pageable pageable ) {
		Optional<Account> account = accountService.findById(accountId);
		if( account.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Account not found"), HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(account.get().getCustomers(), HttpStatus.OK);
	}
	
	@PutMapping("/accounts/{accountId}/customers/{customerId}")
	public ResponseEntity<?> addCustomerToAccount( @PathVariable Long accountId, @PathVariable Long customerId ) {
		Optional<Account> accountOptional = accountService.findById(accountId);
		if( accountOptional.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Account not found"), HttpStatus.NOT_FOUND);
		}
		
		Optional<Customer> customerOptional = customerService.findById(customerId);
		if( customerOptional.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Customer not found"), HttpStatus.NOT_FOUND);
		}
		
		Account account = accountOptional.get();
		Customer customer = customerOptional.get();
		List<Customer> customers = account.getCustomers();
		
		if(!customers.contains(customer)) {
			customers.add(customer);
			account.setCustomers(customers);
		}
		
		try {
			accountService.save(account);
			return new ResponseEntity<>( account.getCustomers(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/accounts/{accountId}/customers/{customerId}")
	public ResponseEntity<?> removeCustomerFromAccount( @PathVariable Long accountId, @PathVariable Long customerId ) {
		Optional<Account> accountOptional = accountService.findById(accountId);
		if( accountOptional.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Account not found"), HttpStatus.NOT_FOUND);
		}
		
		Optional<Customer> customerOptional = customerService.findById(customerId);
		if( customerOptional.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Customer not found"), HttpStatus.NOT_FOUND);
		}
		
		Account account = accountOptional.get();
		Customer customer = customerOptional.get();
		List<Customer> customers = account.getCustomers();
		
		if(customers.contains(customer)) {
			customers.remove(customer);
			account.setCustomers(customers);
		}else {
			return new ResponseEntity<>(new MessageResponse<>("Customer was not associated with the account"), HttpStatus.NOT_FOUND);
		}
		
		try {
			accountService.save(account);
			return new ResponseEntity<>( account.getCustomers(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/accounts/{accountId}/cards")
	public ResponseEntity<?> getCardsByAccountIdPages( @PathVariable Long accountId, Pageable pageable ) {
		Optional<Account> account = accountService.findById(accountId);
		if( account.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Account not found"), HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(account.get().getCards(), HttpStatus.OK);
	}
}
