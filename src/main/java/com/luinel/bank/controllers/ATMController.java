package com.luinel.bank.controllers;

import static com.luinel.bank.utils.TransactionTypeConstants.ATM_FEE;
import static com.luinel.bank.utils.TransactionTypeConstants.CASH_WITHDRAWAL;
import static com.luinel.bank.utils.TransactionTypeConstants.CASH_DEPOSIT;
import static com.luinel.bank.utils.TransactionTypeConstants.TRANSFER_SENT;
import static com.luinel.bank.utils.TransactionTypeConstants.TRANSFER_RECEIVED;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luinel.bank.entities.Account;
import com.luinel.bank.entities.Bank;
import com.luinel.bank.entities.Card;
import com.luinel.bank.entities.Customer;
import com.luinel.bank.responses.MessageResponse;
import com.luinel.bank.services.AccountService;
import com.luinel.bank.services.BankService;
import com.luinel.bank.services.CardService;
import com.luinel.bank.services.TransactionService;
import com.luinel.bank.utils.Encryption;
import com.luinel.bank.utils.ValidateIBAN;

@RestController
@RequestMapping(path = "/api/v1")
public class ATMController {
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private BankService bankService;
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	private TransactionService transactionService;
	
	@Autowired
	private Encryption encryption;
	
	@Autowired
	private ValidateIBAN validateIban;
	
	@GetMapping("/atms/operations/accounts")
	public ResponseEntity<?> getCustomerAccounts( HttpServletRequest request ) {
		
		Card card = cardService.getCardRegisteredJWT(request);
		
		try {
			return new ResponseEntity<>( card.getCustomer().getAccounts() , HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/atms/operations/accounts/{accountId}/transactions")
	public ResponseEntity<?> getTransactionsByAccountId( HttpServletRequest request, @PathVariable Long accountId, Pageable pageable) {
		
		Optional<Account> accountOpt = accountService.findById(accountId);
		if( accountOpt.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Account not found"), HttpStatus.NOT_FOUND);
		}
		Account account = accountOpt.get();
		
		Card card = cardService.getCardRegisteredJWT(request);
		Customer customer = card.getCustomer();
		
		if( !account.getCustomers().contains(customer) ) {
			return new ResponseEntity<>(new MessageResponse<>("Account transactions cannot be viewed"), HttpStatus.UNAUTHORIZED);
		}
		
		try {
			return new ResponseEntity<>( transactionService.findByAccountIdOrderByCreatedAtDesc(accountId, pageable), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/atms/operations/card/password")
	public ResponseEntity<?> changePassword( HttpServletRequest request , @RequestBody Map<String, String> requestBody ) {
		
		Card card = cardService.getCardRegisteredJWT(request);
		
		if( requestBody != null && requestBody.containsKey("newPin") ) {
			
			String newPin = (String) requestBody.get("newPin");
			
			card.setPin( encryption.encrypt(newPin) );
			card.setUpdatedAt(new Date());
			
			try {
				cardService.save(card);
				return new ResponseEntity<>(new MessageResponse<>("Pin changed successfully."), HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}else{
			return new ResponseEntity<>(new MessageResponse<>("New pin entered by user is not received correctly."), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/atms/operations/card/configuration")
	public ResponseEntity<?> getConfigurationCard( HttpServletRequest request ) {
		
		Card card = cardService.getCardRegisteredJWT(request);
		
		Map<String, Object> configurations = new HashMap<>();
		configurations.put("dailyLimit", card.getDailyLimit());
		
		try {
			return new ResponseEntity<>( configurations , HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/atms/operations/card/configuration")
	public ResponseEntity<?> updateConfigurationCard( HttpServletRequest request, @RequestBody Map<String, Object> params ) {
				
		if( params != null && params.containsKey("dailyLimit")) {
			Card card = cardService.getCardRegisteredJWT(request);
			Bank bank = bankService.getBankRegisteredJWT(request, true);
			Map<String, Object> errors = new HashMap<>();
			
			int dailyLimit = (int) params.get("dailyLimit");
			if( bank.getMinimumDailyLimit() <= dailyLimit && dailyLimit <= bank.getMaximumDailyLimit() ) {
				card.setDailyLimit(dailyLimit);
			}else {
				errors.put("dailyLimit", "Daily limit must be between " + bank.getMinimumDailyLimit() + " and " + bank.getMaximumDailyLimit());
			}
			
			if(!errors.isEmpty()) {
				return new ResponseEntity<>(new MessageResponse<>("Configuration data error", errors), HttpStatus.BAD_REQUEST);
			}
			
			try {
				card.setUpdatedAt(new Date());
				cardService.save(card);
				return new ResponseEntity<>( new MessageResponse<>("Card configuration updated successfully.") , HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}else {
			return new ResponseEntity<>(new MessageResponse<>("Configuration data not received correctly."), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/atms/operations/accounts/withdrawals")
	public ResponseEntity<?> cashWithdrawal( HttpServletRequest request, @RequestBody Map<String, Object> params ) {
		if( params != null && params.containsKey("amount")) {
			Card card = cardService.getCardRegisteredJWT(request);
			Bank cardBank = bankService.getBankRegisteredJWT(request, true);
			Bank atmBank = bankService.getBankRegisteredJWT(request, false);
			Account account = card.getAccount();
			Map<String, Object> errors = new HashMap<>();
			
			int amount = (int) params.get("amount");
			if( amount < 0 ) {
				errors.put("amount", "Amount must be positive.");
			}else if ( amount > account.getBalance() ){
				errors.put("withdrawal", "Insufficient balance.");
			} else if ( !transactionService.dailyLimitAllowed( card.getDailyLimit(), account.getId(), amount ) ){
				errors.put("withdrawal", "Daily withdrawal limit exceeded.");
			}
			
			if(!errors.isEmpty()) {
				return new ResponseEntity<>(new MessageResponse<>("Withdrawal error", errors), HttpStatus.BAD_REQUEST);
			}
			
			try {
				transactionService.registerNewTransaction(amount*-1, account, CASH_WITHDRAWAL );
				
				if( !cardBank.getId().equals( atmBank.getId() ) )
					transactionService.registerNewTransaction( atmBank.getCommissionOtherBanks()*-1, account, ATM_FEE );
				
				accountService.updateBalanceById( account.getId() );
				
				return new ResponseEntity<>( new MessageResponse<>("Successful cash withdrawal.") , HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}else {
			return new ResponseEntity<>(new MessageResponse<>("Amount not received correctly."), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/atms/operations/accounts/deposit")
	public ResponseEntity<?> cashDeposit( HttpServletRequest request, @RequestBody Map<String, Object> params ) {
		if( params != null && params.containsKey("amount")) {
			Card card = cardService.getCardRegisteredJWT(request);
			Bank cardBank = bankService.getBankRegisteredJWT(request, true);
			Bank atmBank = bankService.getBankRegisteredJWT(request, false);
			Account account = card.getAccount();
			Map<String, Object> errors = new HashMap<>();
			
			int amount = (int) params.get("amount");
			if( amount < 0 ) {
				errors.put("amount", "Amount must be positive.");
			}else if ( !cardBank.getId().equals( atmBank.getId() ) ){
				errors.put("deposit", "You cannot deposit money from ATMs of other banks.");
			}
			
			if(!errors.isEmpty()) {
				return new ResponseEntity<>(new MessageResponse<>("Deposit error", errors), HttpStatus.BAD_REQUEST);
			}
			
			try {
				transactionService.registerNewTransaction(amount, account, CASH_DEPOSIT );
				
				accountService.updateBalanceById( account.getId() );
				
				return new ResponseEntity<>( new MessageResponse<>("Successful cash deposit.") , HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}else {
			return new ResponseEntity<>(new MessageResponse<>("Amount not received correctly."), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/atms/operations/accounts/transfer")
	public ResponseEntity<?> bankTransfer( HttpServletRequest request, @RequestBody Map<String, Object> params ) {
		if( params != null && params.containsKey("amount") && params.containsKey("iban") ) {
			Card card = cardService.getCardRegisteredJWT(request);
			Bank cardBank = bankService.getBankRegisteredJWT(request, true);
			Bank atmBank = bankService.getBankRegisteredJWT(request, false);
			Account account = card.getAccount();
			Map<String, Object> errors = new HashMap<>();
			
			int amount = (int) params.get("amount");
			String iban = (String) params.get("iban");
			
			if( amount < 0 ) {
				errors.put("amount", "Amount must be positive.");
			}else if ( amount > account.getBalance() ){
				errors.put("transfer", "Insufficient balance.");
			}else if ( !cardBank.getId().equals( atmBank.getId() ) ){
				errors.put("deposit", "You cannot transfer money from ATMs of other banks.");
			}else if( !validateIban.validIban(iban) ) {
				errors.put("iban", "Invalid IBAN format");
			}
			
			if(!errors.isEmpty()) {
				return new ResponseEntity<>(new MessageResponse<>("Transfer error", errors), HttpStatus.BAD_REQUEST);
			}
			
			Optional<Account> optAccount = accountService.findByIban(iban);
			Account destinationAccount = ( !optAccount.isEmpty() )  ? optAccount.get() : null;
			
			try {
				
				//The sent transfer is recorded.
				transactionService.registerNewTransaction(amount*-1, account, TRANSFER_SENT );
				
				//If the destination account exists in the database
				if(destinationAccount != null) {
					
					//The transfer received is recorded
					transactionService.registerNewTransaction(amount, destinationAccount, TRANSFER_RECEIVED);
					
					//The destination account balance is updated
					accountService.updateBalanceById( destinationAccount.getId() );
					
					//It is necessary to evaluate if it is from another bank to charge commission
					if( !cardBank.getId().equals(destinationAccount.getBranch().getBank().getId()) ){
						transactionService.registerNewTransaction( cardBank.getCommissionOtherBanks()*-1, account, ATM_FEE );
					}
					
				}else {
					//If the account does not exist in the database, it is because it belongs to another bank and you have to charge a commission
					transactionService.registerNewTransaction( cardBank.getCommissionOtherBanks()*-1, account, ATM_FEE );
				}
				
				//the account balance is updated
				accountService.updateBalanceById( account.getId() );
				
				return new ResponseEntity<>( new MessageResponse<>("Successful transfer money.") , HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}else {
			return new ResponseEntity<>(new MessageResponse<>("Input data received correctly."), HttpStatus.BAD_REQUEST);
		}
	}
}
