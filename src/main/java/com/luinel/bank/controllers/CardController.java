package com.luinel.bank.controllers;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luinel.bank.entities.Card;
import com.luinel.bank.responses.MessageResponse;
import com.luinel.bank.services.CardService;
import com.luinel.bank.utils.Encryption;
import com.luinel.bank.utils.ValidateEntity;

@RestController
@RequestMapping(path = "/api/v1")
public class CardController {

	@Autowired
	private CardService cardService;

	@Autowired
	private ValidateEntity validateEntity;
	
	@Autowired
	private Encryption encryption;
	
	@GetMapping("/cards/all")
	public ResponseEntity<?> getAllCards() {
		return new ResponseEntity<>(cardService.findByEnabledTrue(), HttpStatus.OK);
	}

	@GetMapping("/cards")
	public ResponseEntity<?> getCardsByPages(Pageable pageable) {
		return new ResponseEntity<>(cardService.findByEnabledTrueByPages(pageable), HttpStatus.OK);
	}

	@GetMapping("/cards/{id}")
	public ResponseEntity<?> getCard(@PathVariable Long id) {
		Optional<Card> card = cardService.findById(id);
		if (card.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("Card not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(card.get(), HttpStatus.OK);

	}

	@PostMapping("/cards")
	public ResponseEntity<?> createCard(@Valid @RequestBody Card card, BindingResult result) {
		Boolean existsCard = cardService.existsByNumberCard(card.getNumberCard());
		if (result.hasErrors() || existsCard) {

			Map<String, Object> errors = validateEntity.validation(result);
			if (existsCard)
				errors.put("numberCard", "Card number already exists");

			return new ResponseEntity<>(new MessageResponse<>("Error creating card.", errors), HttpStatus.BAD_REQUEST);
		}

		try {
			card.setPin( encryption.encrypt( card.getPin() ) );
			card.setEnabled(true);
			card.setInitialActivation(false);
			card.setDailyLimit(500);
			return new ResponseEntity<>(cardService.save(card), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/cards/{id}")
	public ResponseEntity<?> updateCard(@Valid @RequestBody Card card, BindingResult result, @PathVariable Long id) {
		if (result.hasErrors()) {
			return new ResponseEntity<>(
					new MessageResponse<>("Error updating card.", validateEntity.validation(result)),
					HttpStatus.BAD_REQUEST);
		}

		Optional<Card> cardOptional = cardService.findById(id);
		if (cardOptional.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("Card not found"), HttpStatus.NOT_FOUND);
		} else {
			Optional<Card> otherCard = cardService.findByNumberCard(card.getNumberCard());
			if (otherCard.isPresent() && !otherCard.get().getId().equals(id)) {
				return new ResponseEntity<>(new MessageResponse<>("Card number already exists in another card."),
						HttpStatus.BAD_REQUEST);
			}
		}

		Card cardDB = cardOptional.get();
		cardDB.setNumberCard(card.getNumberCard());
		cardDB.setExpirationDate(card.getExpirationDate());
		cardDB.setCvc(card.getCvc());
		cardDB.setUpdatedAt(new Date());

		try {
			return new ResponseEntity<>(cardService.save(cardDB), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/cards/{id}")
	public ResponseEntity<?> deleteCard(@PathVariable Long id) {
		Optional<Card> cardOptional = cardService.findById(id);
		if (cardOptional.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("Card not found"), HttpStatus.NOT_FOUND);
		}

		Card cardDB = cardOptional.get();
		cardDB.setEnabled(false);
		cardDB.setUpdatedAt(new Date());

		try {
			cardService.save(cardDB);
			return new ResponseEntity<>(new MessageResponse<>("Successfully deleted card."), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
