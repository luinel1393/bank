package com.luinel.bank.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luinel.bank.entities.ATM;
import com.luinel.bank.entities.Card;
import com.luinel.bank.requests.OperationATM;
import com.luinel.bank.responses.MessageResponse;
import com.luinel.bank.security.JWTHelper;
import com.luinel.bank.services.ATMService;
import com.luinel.bank.services.BankService;
import com.luinel.bank.services.CardService;
import com.luinel.bank.utils.Encryption;

@RestController
@RequestMapping(path = "/api/v1")
public class AuthController {
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	private ATMService atmService;
	
	@Autowired
	private BankService bankService;
	
	@Autowired
	private Encryption encryption;
	
	@Autowired
	private JWTHelper jwtHelper;
	
	@PostMapping("/atms/operations/login")
	public ResponseEntity<?> login(@RequestBody @Valid OperationATM<?> requestBody) {
		
		Optional<Card> cardOpt = cardService.findById(requestBody.getCardId());
		if (cardOpt.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("Card not found"), HttpStatus.NOT_FOUND);
		}
		
		Optional<ATM> atmOpt = atmService.findById(requestBody.getAtmId());
		if (atmOpt.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("ATM not found"), HttpStatus.NOT_FOUND);
		}
		
		Map<String, ?> params = requestBody.getData();
		if( params != null && params.containsKey("pin")) {
			
			String pinEntered = (String) params.get("pin");
			Card card = cardOpt.get();
			ATM atm = atmOpt.get();
			
			String resultValidCard = validCard( card, pinEntered );
			if( resultValidCard == "valid" ) {
				
				Map<String, Object> claims = new HashMap<>();
				claims.put("card_id", card.getId());
				claims.put("card_bank_id", bankService.findBankIdByCardId( card.getId() ) );
				claims.put("atm_id", atm.getId());
				claims.put("atm_bank_id", bankService.findBankIdByAtmId( atm.getId() ) );
				
				String token = jwtHelper.generateJWTToken( card.getNumberCard(), claims );		
				return new ResponseEntity<>(new MessageResponse<>("Token generated successfully.", token), HttpStatus.OK);
			
			}else if( resultValidCard == "initialActivation" ) {
				return new ResponseEntity<>(new MessageResponse<>("The card has not been activated."), HttpStatus.UNAUTHORIZED);
			}else {
				return new ResponseEntity<>(new MessageResponse<>("Invalid card or pin."), HttpStatus.UNAUTHORIZED);
			}
			
		}else{
			return new ResponseEntity<>(new MessageResponse<>("Pin entered by user is not received correctly."), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/atms/operations/card/activate")
	public ResponseEntity<?> activateCard(@RequestBody @Valid OperationATM<?> requestBody) {
		
		Optional<Card> cardOpt = cardService.findById(requestBody.getCardId());
		if (cardOpt.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("Card not found"), HttpStatus.NOT_FOUND);
		}
		
		Optional<ATM> atmOpt = atmService.findById(requestBody.getAtmId());
		if (atmOpt.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("ATM not found"), HttpStatus.NOT_FOUND);
		}
		
		Map<String, ?> params = requestBody.getData();
		if( params != null && params.containsKey("pin") && params.containsKey("newPin") ) {
			
			String pinEntered = (String) params.get("pin");
			String newPin = (String) params.get("newPin");
			Card card = cardOpt.get();
			
			String resultValidCard = validCard( card, pinEntered );
			if( resultValidCard == "initialActivation" ) {
				
				card.setInitialActivation(true);
				card.setPin( encryption.encrypt(newPin) );
				card.setUpdatedAt(new Date());
				
				try {
					cardService.save(card);
					return new ResponseEntity<>(new MessageResponse<>("Card activated successfully."), HttpStatus.OK);
				} catch (Exception e) {
					return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
				}
			
			}else {
				return new ResponseEntity<>(new MessageResponse<>("Invalid card or pin."), HttpStatus.UNAUTHORIZED);
			}
			
		}else{
			return new ResponseEntity<>(new MessageResponse<>("Pin entered by user is not received correctly."), HttpStatus.BAD_REQUEST);
		}
	}
	
	private String validCard( Card card, String pinEntered ) {
		if ( card.isEnabled() && card.isInitialActivation() && encryption.matching(pinEntered, card.getPin()) ) {
			return "valid";
		}else if( card.isEnabled() && !card.isInitialActivation() && encryption.matching(pinEntered, card.getPin() ) ){
			return "initialActivation";
		}else {
			return "invalid";
		}
	}

}
