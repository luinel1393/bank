package com.luinel.bank.controllers;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luinel.bank.entities.Customer;
import com.luinel.bank.responses.MessageResponse;
import com.luinel.bank.services.CustomerService;
import com.luinel.bank.utils.ValidateEntity;

@RestController
@RequestMapping(path = "/api/v1")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private ValidateEntity validateEntity;

	@GetMapping("/customers/all")
	public ResponseEntity<?> getAllCustomers() {
		return new ResponseEntity<>(customerService.findByEnabledTrue(), HttpStatus.OK);
	}

	@GetMapping("/customers")
	public ResponseEntity<?> getCustomersByPages(Pageable pageable) {
		return new ResponseEntity<>(customerService.findByEnabledTrueByPages(pageable), HttpStatus.OK);
	}

	@GetMapping("/customers/{id}")
	public ResponseEntity<?> getCustomer(@PathVariable Long id) {
		Optional<Customer> customer = customerService.findById(id);
		if (customer.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("Customer not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(customer.get(), HttpStatus.OK);
	}

	@PostMapping("/customers")
	public ResponseEntity<?> createCustomer(@Valid @RequestBody Customer customer, BindingResult result) {
		
		Boolean existsCustomer = customerService.existsByDocument( customer.getDocument() );
		if (result.hasErrors() || existsCustomer ) {
			
			Map<String, Object> errores = validateEntity.validation(result);
			if(existsCustomer) errores.put("document", "Document already exists");
			
			return new ResponseEntity<>( new MessageResponse<>("Error creating customer.", errores), HttpStatus.BAD_REQUEST);
		}

		try {
			customer.setEnabled(true);
			return new ResponseEntity<>( customerService.save(customer), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/customers/{id}")
	public ResponseEntity<?> updateCustomer(@Valid @RequestBody Customer customer, BindingResult result, @PathVariable Long id) {
		if (result.hasErrors() ) {
			return new ResponseEntity<>( new MessageResponse<>("Error updating customer.", validateEntity.validation(result) ), HttpStatus.BAD_REQUEST);
		}

		Optional<Customer> customerOptional = customerService.findById(id);
		if (customerOptional.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("Customer not found"), HttpStatus.NOT_FOUND);
		}else {
			Optional<Customer> otherCustomer = customerService.findByDocument(customer.getDocument());
			if( otherCustomer.isPresent() && !otherCustomer.get().getId().equals( id ) ) {
				return new ResponseEntity<>(new MessageResponse<>("Document already exists in another customer."), HttpStatus.BAD_REQUEST);
			}
		}

		Customer customerDB = customerOptional.get();
		customerDB.setName(customer.getName());
		customerDB.setLastname(customer.getLastname());
		customerDB.setDocument(customer.getDocument());
		customerDB.setPhone(customer.getPhone());
		customerDB.setAddress(customer.getAddress());
		customerDB.setUpdatedAt(new Date());
		
		try {
			return new ResponseEntity<>( customerService.save(customerDB) , HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/customers/{id}")
	public ResponseEntity<?> deleteCustomer(@PathVariable Long id) {
		Optional<Customer> customerOptional = customerService.findById(id);
		if (customerOptional.isEmpty()) {
			return new ResponseEntity<>(new MessageResponse<>("Customer not found"), HttpStatus.NOT_FOUND);
		}
		
		Customer customerDB = customerOptional.get();
		customerDB.setEnabled(false);
		customerDB.setUpdatedAt(new Date());
		
		try {
			customerService.save(customerDB);
			return new ResponseEntity<>( new MessageResponse<>("Successfully deleted customer."), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse<>("Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/{customerId}/accounts")
	public ResponseEntity<?> getAccountsByCustomerIdPages( @PathVariable Long customerId, Pageable pageable ) {
		Optional<Customer> customer = customerService.findById(customerId);
		if( customer.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Customer not found"), HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(customer.get().getAccounts(), HttpStatus.OK);
	}
	
	@GetMapping("/customers/{customerId}/cards")
	public ResponseEntity<?> getCardsByCustomerIdPages( @PathVariable Long customerId, Pageable pageable ) {
		Optional<Customer> customer = customerService.findById(customerId);
		if( customer.isEmpty() ) {
			return new ResponseEntity<>(new MessageResponse<>("Customer not found"), HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(customer.get().getCards(), HttpStatus.OK);
	}

}
