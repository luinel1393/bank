package com.luinel.bank.requests;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

public class OperationATM<T> {
	
	private Long cardId;
	
	private Long atmId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Map<String, T> data;

	public Long getCardId() {
		return cardId;
	}

	public void setCardId(Long cardId) {
		this.cardId = cardId;
	}

	public Long getAtmId() {
		return atmId;
	}

	public void setAtmId(Long atmId) {
		this.atmId = atmId;
	}

	public Map<String, T> getData() {
		return data;
	}

	public void setData(Map<String, T> data) {
		this.data = data;
	}

	
	
	
}
