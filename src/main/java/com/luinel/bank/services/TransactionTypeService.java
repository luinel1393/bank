package com.luinel.bank.services;

import java.util.Optional;

import com.luinel.bank.entities.TransactionType;

public interface TransactionTypeService {
	public Iterable<TransactionType> findAll();

	public Optional<TransactionType> findById(Long id);

	public TransactionType save(TransactionType transaction_type);

	public void delete(Long id);
}
