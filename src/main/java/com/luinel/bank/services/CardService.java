package com.luinel.bank.services;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.luinel.bank.entities.Card;

public interface CardService {
	public Iterable<Card> findAll();
	
	public Page<Card> findAllByPages( Pageable pageable);
	
	public Iterable<Card> findByEnabledTrue();
	
	public Page<Card> findByEnabledTrueByPages( Pageable pageable);

	public Optional<Card> findById(Long id);

	public Card save(Card card);

	public void delete(Long id);
	
	public Boolean existsByNumberCard(String numberCard);
	
	public Long findIdByNumberCard(String numberCard);
	
	public Optional<Card> findByNumberCard(String numberCard);
	
	public Card getCardRegisteredJWT( HttpServletRequest request );

}
