package com.luinel.bank.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.luinel.bank.entities.Account;

public interface AccountService {

	public Iterable<Account> findAll();
	
	public Page<Account> findAllByPages( Pageable pageable );
	
	public Iterable<Account> findByEnabledTrue();
	
	public Page<Account> findByEnabledTrueByPages( Pageable pageable);
	
	public Page<Account> findByBranchIdAndEnabledTrue( Long branchId, Pageable pageable);

	public Optional<Account> findById(Long id);

	public Account save(Account account);

	public void delete(Long id);
	
	public Boolean existsByIban(String iban);
	
	public Optional<Account> findByIban(String iban);
	
	public void updateBalanceById(Long accountId);
}
