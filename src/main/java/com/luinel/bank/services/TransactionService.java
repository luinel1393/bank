package com.luinel.bank.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.luinel.bank.entities.Account;
import com.luinel.bank.entities.Transaction;

public interface TransactionService {
	public Iterable<Transaction> findAll();

	public Optional<Transaction> findById(Long id);

	public Transaction save(Transaction transaction);

	public void delete(Long id);
	
	public Page<Transaction> findByAccountIdOrderByCreatedAtDesc( Long idAccount, Pageable pageable );
	
	public int findTotalDailyByAccountIdAndTransactionTypeDescriptionAndDateRange(Long accountId, String transactionTypeDescription, Date startDate, Date endDate);
	
	public Boolean dailyLimitAllowed(int dailyLimit, Long accountId, int amount);
	
	public void registerNewTransaction(int amount, Account account, String transactionTypeDescription);
}
