package com.luinel.bank.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.luinel.bank.entities.Customer;

public interface CustomerService {
	public Iterable<Customer> findAll();
	
	public Page<Customer> findAllByPages( Pageable pageable);
	
	public Iterable<Customer> findByEnabledTrue();
	
	public Page<Customer> findByEnabledTrueByPages( Pageable pageable);

	public Optional<Customer> findById(Long id);

	public Customer save(Customer customer);

	public void delete(Long id);
	
	public Boolean existsByDocument(String document);
	
	public Optional<Customer> findByDocument(String document);
}
