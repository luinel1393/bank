package com.luinel.bank.services;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import com.luinel.bank.entities.Bank;

public interface BankService {
	public Iterable<Bank> findAll();

	public Optional<Bank> findById(Long id);

	public Bank save(Bank bank);

	public void delete(Long id);
	
	public Long findBankIdByCardId(Long cardId);
	
	public Long findBankIdByAtmId(Long atmId);
	
	public Bank getBankRegisteredJWT(HttpServletRequest request, Boolean cardBank);
}
