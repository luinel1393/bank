package com.luinel.bank.services.impl;

import static com.luinel.bank.utils.TransactionTypeConstants.CASH_WITHDRAWAL;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.bank.entities.Account;
import com.luinel.bank.entities.Transaction;
import com.luinel.bank.entities.TransactionType;
import com.luinel.bank.repositories.TransactionRepository;
import com.luinel.bank.repositories.TransactionTypeRepository;
import com.luinel.bank.services.TransactionService;
import com.luinel.bank.utils.Dates;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;
	
	@Autowired
	private TransactionTypeRepository transactionTypeRepository;

	@Autowired
	private Dates dates;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Transaction> findAll() {
		return transactionRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Transaction> findById(Long id) {
		return transactionRepository.findById(id);
	}

	@Override
	@Transactional
	public Transaction save(Transaction transaction) {
		return transactionRepository.save(transaction);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		transactionRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Transaction> findByAccountIdOrderByCreatedAtDesc(Long accountId, Pageable pageable) {
		return transactionRepository.findByAccountIdOrderByCreatedAtDesc(accountId, pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public int findTotalDailyByAccountIdAndTransactionTypeDescriptionAndDateRange(Long accountId,
			String transactionTypeDescription, Date startDate, Date endDate) {
		return transactionRepository.findTotalDailyByAccountIdAndTransactionTypeDescriptionAndDateRange(accountId,
				transactionTypeDescription, startDate, endDate);
	}

	@Override
	@Transactional(readOnly = true)
	public Boolean dailyLimitAllowed(int dailyLimit, Long accountId, int amount) {

		int dailyWithdrawal = findTotalDailyByAccountIdAndTransactionTypeDescriptionAndDateRange(accountId,
				CASH_WITHDRAWAL, dates.getCurrentDateMinimumTime(), dates.getCurrentDateMaximumTime());
		
		dailyWithdrawal = dailyWithdrawal * -1;

		return (dailyWithdrawal + amount <= dailyLimit) ? true : false;
	}

	@Override
	public void registerNewTransaction(int amount, Account account, String transactionTypeDescription) {
		
		Optional<TransactionType> opt = transactionTypeRepository.findByDescription(transactionTypeDescription);
		TransactionType transactionType = null;
		if(!opt.isEmpty()) transactionType = opt.get();
		
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setAccount(account);
		transaction.setTransactionType(transactionType);
		save(transaction);
	}

}
