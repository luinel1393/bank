package com.luinel.bank.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.bank.entities.TransactionType;
import com.luinel.bank.repositories.TransactionTypeRepository;
import com.luinel.bank.services.TransactionTypeService;

@Service
public class TransactionTypeServiceImpl implements TransactionTypeService {

	@Autowired
	private TransactionTypeRepository transaction_typeRepository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<TransactionType> findAll() {
		return transaction_typeRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<TransactionType> findById(Long id) {
		return transaction_typeRepository.findById(id);
	}

	@Override
	@Transactional
	public TransactionType save(TransactionType transaction_type) {
		return transaction_typeRepository.save(transaction_type);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		transaction_typeRepository.deleteById(id);
	}

}
