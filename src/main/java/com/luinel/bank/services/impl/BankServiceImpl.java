package com.luinel.bank.services.impl;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.bank.entities.Bank;
import com.luinel.bank.repositories.BankRepository;
import com.luinel.bank.security.JWTHelper;
import com.luinel.bank.services.BankService;

@Service
public class BankServiceImpl implements BankService {

	@Autowired
	private BankRepository bankRepository;
	
	@Autowired
	private JWTHelper jwtHelper;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Bank> findAll() {
		return bankRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Bank> findById(Long id) {
		return bankRepository.findById(id);
	}

	@Override
	@Transactional
	public Bank save(Bank bank) {
		return bankRepository.save(bank);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		bankRepository.deleteById(id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long findBankIdByCardId(Long cardId) {
		return bankRepository.findBankIdByCardId(cardId);
	}

	@Override
	@Transactional(readOnly = true)
	public Long findBankIdByAtmId(Long atmId) {
		return bankRepository.findBankIdByAtmId(atmId);
	}

	@Override
	@Transactional(readOnly = true)
	public Bank getBankRegisteredJWT(HttpServletRequest request, Boolean cardBank) {
		String key = cardBank ? "card_bank_id" : "atm_bank_id";
		Long bankId = Long.valueOf( jwtHelper.getClaimValueFromToken(request, key) );
		Optional<Bank> bankOpt = findById(bankId);
		return bankOpt.get();
	}
}
