package com.luinel.bank.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.bank.entities.CardType;
import com.luinel.bank.repositories.CardTypeRepository;
import com.luinel.bank.services.CardTypeService;

@Service
public class CardTypeServiceImpl implements CardTypeService {

	@Autowired
	private CardTypeRepository card_typeRepository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<CardType> findAll() {
		return card_typeRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<CardType> findById(Long id) {
		return card_typeRepository.findById(id);
	}

	@Override
	@Transactional
	public CardType save(CardType card_type) {
		return card_typeRepository.save(card_type);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		card_typeRepository.deleteById(id);
	}

}
