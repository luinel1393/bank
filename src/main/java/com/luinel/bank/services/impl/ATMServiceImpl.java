package com.luinel.bank.services.impl;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.bank.entities.ATM;
import com.luinel.bank.repositories.ATMRepository;
import com.luinel.bank.security.JWTHelper;
import com.luinel.bank.services.ATMService;

@Service
public class ATMServiceImpl implements ATMService {

	@Autowired
	private ATMRepository atmRepository;
	
	@Autowired
	private JWTHelper jwtHelper;

	@Override
	@Transactional(readOnly = true)
	public Iterable<ATM> findAll() {
		return atmRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<ATM> findById(Long id) {
		return atmRepository.findById(id);
	}

	@Override
	@Transactional
	public ATM save(ATM atm) {
		return atmRepository.save(atm);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		atmRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<ATM> findByBranchId(Long branchId, Pageable pageable) {
		return atmRepository.findByBranchId(branchId,pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public ATM getATMRegisteredJWT(HttpServletRequest request) {
		Long atmId = Long.valueOf(jwtHelper.getClaimValueFromToken(request, "atm_id"));
		Optional<ATM> atmOpt = findById(atmId);
		return atmOpt.get();
	}

}
