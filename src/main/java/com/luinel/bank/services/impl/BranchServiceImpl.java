package com.luinel.bank.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.bank.entities.Branch;
import com.luinel.bank.repositories.BranchRepository;
import com.luinel.bank.services.BranchService;

@Service
public class BranchServiceImpl implements BranchService {

	@Autowired
	private BranchRepository branchRepository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Branch> findAll() {
		return branchRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Branch> findById(Long id) {
		return branchRepository.findById(id);
	}

	@Override
	@Transactional
	public Branch save(Branch branch) {
		return branchRepository.save(branch);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		branchRepository.deleteById(id);
	}

}
