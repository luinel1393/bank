package com.luinel.bank.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.bank.entities.Customer;
import com.luinel.bank.repositories.CustomerRepository;
import com.luinel.bank.services.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Customer> findAll() {
		return customerRepository.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<Customer> findAllByPages( Pageable pageable) {
		return customerRepository.findAll( pageable );
	}
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Customer> findByEnabledTrue() {
		return customerRepository.findByEnabledTrue();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<Customer> findByEnabledTrueByPages(Pageable pageable) {
		return customerRepository.findByEnabledTrue(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Customer> findById(Long id) {
		return customerRepository.findById(id);
	}

	@Override
	@Transactional
	public Customer save(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		customerRepository.deleteById(id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Boolean existsByDocument( String document) {
		return customerRepository.existsByDocument( document );
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Customer> findByDocument(String document) {
		return customerRepository.findByDocument(document);
	}
}
