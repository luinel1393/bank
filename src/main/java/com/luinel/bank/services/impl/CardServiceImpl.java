package com.luinel.bank.services.impl;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.bank.entities.Card;
import com.luinel.bank.repositories.CardRepository;
import com.luinel.bank.security.JWTHelper;
import com.luinel.bank.services.CardService;

@Service
public class CardServiceImpl implements CardService {

	@Autowired
	private CardRepository cardRepository;
	
	@Autowired
	private JWTHelper jwtHelper;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Card> findAll() {
		return cardRepository.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<Card> findAllByPages( Pageable pageable) {
		return cardRepository.findAll( pageable );
	}
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Card> findByEnabledTrue() {
		return cardRepository.findByEnabledTrue();
	}
	
	@Override
	public Page<Card> findByEnabledTrueByPages(Pageable pageable) {
		return cardRepository.findByEnabledTrue(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Card> findById(Long id) {
		return cardRepository.findById(id);
	}

	@Override
	@Transactional
	public Card save(Card card) {
		return cardRepository.save(card);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		cardRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Boolean existsByNumberCard( String numberCard) {
		return cardRepository.existsByNumberCard(numberCard);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long findIdByNumberCard( String numberCard) {
		return cardRepository.findIdByNumberCard(numberCard);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Card> findByNumberCard(String numberCard) {
		return cardRepository.findByNumberCard(numberCard);
	}

	@Override
	@Transactional(readOnly = true)
	public Card getCardRegisteredJWT(HttpServletRequest request) {
		Long cardId = Long.valueOf( jwtHelper.getClaimValueFromToken(request, "card_id") );
		Optional<Card> cardOpt = findById(cardId);
		return cardOpt.get();
	}
	
	
}
