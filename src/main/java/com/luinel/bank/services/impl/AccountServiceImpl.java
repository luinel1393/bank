package com.luinel.bank.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.bank.entities.Account;
import com.luinel.bank.repositories.AccountRepository;
import com.luinel.bank.services.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Account> findAll() {
		return accountRepository.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<Account> findAllByPages(Pageable pageable) {
		return accountRepository.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Account> findByEnabledTrue() {
		return accountRepository.findByEnabledTrue();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<Account> findByEnabledTrueByPages(Pageable pageable) {
		return accountRepository.findByEnabledTrue(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<Account> findByBranchIdAndEnabledTrue(Long branchId, Pageable pageable) {
		return accountRepository.findByBranchIdAndEnabledTrue(branchId,pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Account> findById(Long id) {
		return accountRepository.findById(id);
	}

	@Override
	@Transactional
	public Account save(Account account) {
		return accountRepository.save(account);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		accountRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Boolean existsByIban( String iban) {
		return accountRepository.existsByIban( iban );
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Account> findByIban(String iban) {
		return accountRepository.findByIban(iban);
	}

	@Override
	@Transactional
	public void updateBalanceById(Long accountId) {
		accountRepository.updateBalanceById(accountId);
	}
}
