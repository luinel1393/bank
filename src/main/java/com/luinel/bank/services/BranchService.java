package com.luinel.bank.services;

import java.util.Optional;

import com.luinel.bank.entities.Branch;

public interface BranchService {
	public Iterable<Branch> findAll();

	public Optional<Branch> findById(Long id);

	public Branch save(Branch branch);

	public void delete(Long id);
}
