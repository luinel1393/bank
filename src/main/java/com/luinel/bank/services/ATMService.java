package com.luinel.bank.services;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.luinel.bank.entities.ATM;

public interface ATMService {
	public Iterable<ATM> findAll();

	public Optional<ATM> findById(Long id);

	public ATM save(ATM atm);

	public void delete(Long id);
	
	public Page<ATM> findByBranchId( Long branchId, Pageable pageable);
	
	public ATM getATMRegisteredJWT( HttpServletRequest request );
}
