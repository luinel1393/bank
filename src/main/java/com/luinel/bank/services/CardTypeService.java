package com.luinel.bank.services;

import java.util.Optional;

import com.luinel.bank.entities.CardType;

public interface CardTypeService {
	public Iterable<CardType> findAll();

	public Optional<CardType> findById(Long id);

	public CardType save(CardType card_type);

	public void delete(Long id);
}
