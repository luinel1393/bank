package com.luinel.bank.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.luinel.bank.entities.Bank;

@Repository
public interface BankRepository extends CrudRepository<Bank, Long> {

	@Query("select b.id from Bank b join b.branches br join br.accounts a join a.cards c where c.id = ?1")
	public Long findBankIdByCardId(Long cardId);
	
	@Query("select b.id from Bank b join b.branches br join br.atms a where a.id = ?1")
	public Long findBankIdByAtmId(Long atmId);
}
