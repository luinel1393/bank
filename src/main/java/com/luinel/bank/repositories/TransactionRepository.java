package com.luinel.bank.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.luinel.bank.entities.Transaction;

@Repository
public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Long> {
	
	public Page<Transaction >findByAccountIdOrderByCreatedAtDesc(Long idAccount, Pageable pageable);
	
	@Query("select coalesce(sum(t.amount),0) from Transaction t where t.account.id = ?1 and t.transactionType.description = ?2 and t.createdAt >= ?3")
	public int findTotalDailyByAccountIdAndTransactionTypeDescriptionAndDateRange( Long accountId, String transactionTypeDescription, Date startDate, Date endDate );

}
