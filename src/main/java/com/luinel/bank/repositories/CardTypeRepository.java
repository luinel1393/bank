package com.luinel.bank.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.luinel.bank.entities.CardType;

@Repository
public interface CardTypeRepository extends CrudRepository<CardType, Long> {

}
