package com.luinel.bank.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.luinel.bank.entities.Customer;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {
	
	public Iterable<Customer> findByEnabledTrue();
	
	public Page<Customer> findByEnabledTrue(Pageable pageable);
	
	public Boolean existsByDocument(String dcument);
	
	public Optional<Customer> findByDocument(String document);

}
