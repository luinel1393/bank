package com.luinel.bank.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.luinel.bank.entities.TransactionType;

@Repository
public interface TransactionTypeRepository extends CrudRepository<TransactionType, Long> {

	public Optional<TransactionType> findByDescription(String description);
}
