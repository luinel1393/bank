package com.luinel.bank.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.luinel.bank.entities.ATM;

@Repository
public interface ATMRepository extends PagingAndSortingRepository<ATM, Long> {
	
	public Page<ATM> findByBranchId(Long branchId, Pageable pageable);

}
