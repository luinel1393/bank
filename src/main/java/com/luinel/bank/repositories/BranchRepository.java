package com.luinel.bank.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.luinel.bank.entities.Branch;

@Repository
public interface BranchRepository extends CrudRepository<Branch, Long> {

}
