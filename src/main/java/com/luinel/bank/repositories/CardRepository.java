package com.luinel.bank.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.luinel.bank.entities.Card;

@Repository
public interface CardRepository extends PagingAndSortingRepository<Card, Long> {
	
	public Iterable<Card> findByEnabledTrue();
	
	public Page<Card> findByEnabledTrue(Pageable pageable);
	
	public Boolean existsByNumberCard(String numberCard);
	
	public Long findIdByNumberCard(String numberCard);
	
	public Optional<Card> findByNumberCard(String numberCard);

}
