package com.luinel.bank.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.luinel.bank.entities.Account;

@Repository
public interface AccountRepository extends PagingAndSortingRepository<Account, Long> {

	public Iterable<Account> findByEnabledTrue();
	
	public Page<Account> findByEnabledTrue(Pageable pageable);
	
	public Page<Account> findByBranchIdAndEnabledTrue(Long branchId, Pageable pageable);
	
	public Boolean existsByIban(String iban);
	
	public Optional<Account> findByIban(String iban);
	
	@Modifying
	@Query(nativeQuery = true, value ="UPDATE accounts a SET a.updated_at = NOW(), a.balance = ("
			+ " SELECT coalesce(sum(t.amount),0) from transactions t where t.account_id = ?1"
			+ ") WHERE a.id = ?1")
	public void updateBalanceById( Long accountId );
}
